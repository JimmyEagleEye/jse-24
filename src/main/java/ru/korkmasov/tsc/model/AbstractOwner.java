package ru.korkmasov.tsc.model;

import ru.korkmasov.tsc.api.entity.ITWBS;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractOwner extends AbstractEntity implements ITWBS {

    private String userId;

}

