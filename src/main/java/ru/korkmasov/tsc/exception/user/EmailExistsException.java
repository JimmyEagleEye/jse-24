package ru.korkmasov.tsc.exception.user;

import ru.korkmasov.tsc.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email is already in use...");
    }

}
