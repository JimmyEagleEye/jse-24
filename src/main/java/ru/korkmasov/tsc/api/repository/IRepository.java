package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.model.AbstractEntity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IRepository<E extends AbstractEntity> {

    void add(@Nullable E entity);

    @Nullable
    E findById(@NotNull String id);

    void remove(@Nullable E entity);

    @Nullable
    E removeById(@NotNull String id);

    int size();

}
