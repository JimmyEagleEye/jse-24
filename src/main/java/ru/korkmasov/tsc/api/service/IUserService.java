package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.model.User;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public interface IUserService extends IService<User> {

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

    @NotNull
    User add(@NotNull String login, @NotNull String password);

    @NotNull
    User add(@NotNull String login, @NotNull String password, String email);

    @NotNull
    User setPassword(@NotNull String id, @NotNull String password);

    @Nullable
    User updateUser(
            @NotNull String id,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String middleName
    );

    boolean existsByLogin(@NotNull String login);
}
