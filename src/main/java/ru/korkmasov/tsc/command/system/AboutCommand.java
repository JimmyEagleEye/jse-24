package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Djalal Korkmasov");
        System.out.println("dkorkmasov@t1-consulting.com");
    }
}
