package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;

public final class TaskByIdUnbindCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-unbind-by-id";
    }

    @Override
    public String description() {
        return "Unbind task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK BY ID]");
        if (serviceLocator.getTaskService().size(userId) < 1) throw new TaskNotFoundException();
        System.out.println("ENTER TASK ID:");
        serviceLocator.getProjectTaskService().unassignTaskByProjectId(userId, TerminalUtil.nextLine());
    }

}
