package ru.korkmasov.tsc.enumerated;

import ru.korkmasov.tsc.exception.entity.StatusNotFoundException;
import ru.korkmasov.tsc.util.ValidationUtil;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    private final String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public static void main(String[] args) {
        System.out.println(COMPLETE);
    }

    public static Status getStatus(String s) {
        s = s.toUpperCase();
        if (!ValidationUtil.checkStatus(s)) throw new StatusNotFoundException();
        return Status.valueOf(s);
    }

    public String getDisplayName() {
        return displayName;
    }

}




